package models

import (
	"github.com/jinzhu/gorm"
)

type Product struct {
	Code  string `json:"code"`
	Price uint   `json:"price"`
}

type ProductDbEntity struct {
	gorm.Model
	Product
}

type ProductInput struct {
	Product
}
