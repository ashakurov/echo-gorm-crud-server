package controlllers

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"simple-crud/models"
	database "simple-crud/modules"
)

func ListProducts(c echo.Context) (err error) {
	var products []models.ProductDbEntity
	err = database.GetDatabase().Find(&products).Error
	if err != nil {
		return err
	}
	err = c.JSON(http.StatusOK, products)
	return err
}

func GetProduct(c echo.Context) (err error) {
	var product models.ProductDbEntity
	id := c.Param("id")
	err = database.GetDatabase().First(&product, id).Error
	if err != nil {
		return err
	}
	err = c.JSON(http.StatusOK, &product)
	return err
}

func CreateProduct(c echo.Context) (err error) {
	product := new(models.ProductDbEntity)
	if err := c.Bind(product); err != nil {
		return err
	}
	err = database.GetDatabase().Create(product).Error
	if err != nil {
		return err
	}
	err = c.JSON(http.StatusOK, *product)
	return err
}

func UpdateProduct(c echo.Context) (err error) {
	var product models.ProductDbEntity

	id := c.Param("id")
	err = database.GetDatabase().First(&product, id).Error
	if err != nil {
		return err
	}

	var productInput models.ProductInput
	if err := c.Bind(&productInput); err != nil {
		return err
	}

	product.Code = productInput.Code
	product.Price = productInput.Price
	err = database.GetDatabase().Save(&product).Error
	if err != nil {
		return err
	}
	err = c.JSON(http.StatusOK, &product)
	return err
}

func DeleteProduct(c echo.Context) (err error) {
	var product models.ProductDbEntity

	id := c.Param("id")
	err = database.GetDatabase().First(&product, id).Error
	if err != nil {
		return err
	}

	err = database.GetDatabase().Delete(&product).Error
	if err != nil {
		return err
	}
	err = c.JSON(http.StatusOK, &product)
	return err
}
