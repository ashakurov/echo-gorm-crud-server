package modules

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"simple-crud/models"
)

var database *gorm.DB

func GetDatabase() *gorm.DB {
	return database
}

func InitDatabase() error {
	var err error
	database, err = gorm.Open("mysql", "root:root@/crud?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		return err
	}

	migrate()

	return nil
}

func CloseDatabase() error {
	var err error
	err = database.Close()
	if err != nil {
		return err
	}

	return nil
}

func migrate() {
	// Migrate the schema
	database.AutoMigrate(&models.ProductDbEntity{})
}
