package main

import (
	"github.com/labstack/echo/v4"
	"simple-crud/controlllers"
	database "simple-crud/modules"
)

func main() {
	err := database.InitDatabase()
	if err != nil {
		panic("Cannot connect to the database.")
	}
	defer database.CloseDatabase()

	e := echo.New()
	e.GET("/products/:id", controlllers.GetProduct)
	e.GET("/products", controlllers.ListProducts)
	e.POST("/products", controlllers.CreateProduct)
	e.PUT("/products/:id", controlllers.UpdateProduct)
	e.DELETE("/products/:id", controlllers.DeleteProduct)
	e.Logger.Fatal(e.Start(":1323"))
}
